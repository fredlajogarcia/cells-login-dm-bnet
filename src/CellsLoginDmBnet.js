import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsLoginDmBnet-styles.js';
import { BGADPGrantingTicketsPOST } from '@cells-components/bgadp-granting-tickets/bgadp-granting-tickets.js';

/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-login-dm-bnet></cells-login-dm-bnet>
```

##styling-doc

@customElement cells-login-dm-bnet
*/
export class CellsLoginDmBnet extends LitElement {
  static get is() {
    return 'cells-login-dm-bnet';
  }

  // Declare properties
  static get properties() {
    return {
      name: { type: String, },
      host: { type: String, },
      path: { type: String, },
      consumerID: { type: String, },
      token: { type: Object, },

    };
  }

  // Initialize properties
  constructor() {
    super();
    this.name = 'Cells';
    this.host = 'https://glomo.bbva.pe';
    this.path = '/SRVS_A02';
    this.consumerID = '13000013';

  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-login-dm-bnet-shared-styles')
    ];
  }

  // Define a template
  render() {
    return html`
    `;
  }

  login(evt) {
    let gt=new BGADPGrantingTicketsPOST('pe', {
      host: this.host + this.path ,
      version: 0,
      }); 
    gt.generateRequest(true, {
        userId: evt.userId ,
        password: evt.password ,
        consumerId: this.consumerID
    }).then(success => {
        this.requestSuccess(success);
      },error => {
        this.requestError(error);
      }
    );
  }

  requestSuccess(evt){
    this.dispatchEvent(new CustomEvent('login-success', {
      detail: true,
      bubbles: true,
      composed: true,
    }));
  }

  requestError(evt){
    this.dispatchEvent(new CustomEvent('login-error', {
      detail: evt,
      bubbles: true,
      composed: true,
    }));
  }


}
